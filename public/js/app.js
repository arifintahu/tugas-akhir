// chartjs

function ajax(url, method, payload, successCallback){
  var xhr = new XMLHttpRequest();
  xhr.open(method, url, true);
  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xhr.onreadystatechange = function () {
    if (xhr.readyState != 4 || xhr.status != 200) return;
    successCallback(xhr.responseText);
  };
  xhr.send(JSON.stringify(payload));
}


function renderFirstChart(firstData) {
  var ctx = document.getElementById("firstChart").getContext("2d");
  firstChartRef = new Chart(ctx, {
    type: "line",
    data: firstData,
    options:{
      legend : {
        display: false,
        position: 'right'
      },
      scales:{
        yAxes:[{
          scaleLabel: {
            display: true,
            labelString: 'Temperature ( ℃ )'
          }
        }],
        xAxes:[{
          scaleLabel: {
            display: true,
            labelString: 'Time'
          }
        }]
      }
    }
  });
}

function renderSecondChart(secondData) {
  var ctx = document.getElementById("secondChart").getContext("2d");
  secondChartRef = new Chart(ctx, {
    type: "line",
    data: secondData,
    options:{
      legend : {
        display: false,
        position: 'right'
      },
      scales:{
        yAxes:[{
          scaleLabel: {
            display: true,
            labelString: 'pH'
          }
        }],
        xAxes:[{
          scaleLabel: {
            display: true,
            labelString: 'Time'
          }
        }]
      }
    }
  });
}

function renderThirdChart(thirdData) {
  var ctx = document.getElementById("thirdChart").getContext("2d");
  thirdChartRef = new Chart(ctx, {
    type: "line",
    data: thirdData,
    options: {
      legend : {
        display: false,
        position: 'right'
      },
      scales:{
        yAxes:[{
          scaleLabel: {
            display: true,
            labelString: 'Turbidity ( NTU )'
          }
        }],
        xAxes:[{
          scaleLabel: {
            display: true,
            labelString: 'Time'
          }
        }]
      }
    }
  });
}

function renderFourthChart(fourthData) {
  var ctx = document.getElementById("fourthChart").getContext("2d");
  fourthChartRef = new Chart(ctx, {
    type: "line",
    data: fourthData,
    options: {
      legend : {
        display: false,
        position: 'right'
      },
      scales:{
        yAxes:[{
          scaleLabel: {
            display: true,
            labelString: 'Conductivity ( ms/cm )'
          }
        }],
        xAxes:[{
          scaleLabel: {
            display: true,
            labelString: 'Time'
          }
        }]
      }
    }
  });
}

var chartConfig = {
  labels: [],
  datasets: [
    {
        label: "Temperature(Celcius)",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "rgba(75,192,192,1)",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: "rgba(75,192,192,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [],
        spanGaps: false,
    }
  ]
};

var chartConfig2 = {
  labels: [],
  datasets: [
    {
        label: "pH",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(192,75,75,0.4)",
        borderColor: "rgba(192,75,75,1)",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: "rgba(192,75,75,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(192,75,75,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [],
        spanGaps: false,
    }
  ]
  };

var chartConfig3 = {
  labels: [],
  datasets: [
    {
      label: "Turbidity",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(92,175,75,0.4)",
      borderColor: "rgba(92,175,75,1)",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "rgba(92,175,75,1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(92,175,75,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [],
      spanGaps: false,
    }
  ]
};

var chartConfig4 = {
labels: [],
datasets: [
    {
        label: "Conductivity",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "rgba(92,75,175,0.4)",
        borderColor: "rgba(92,75,175,1)",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: "rgba(92,75,175,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(92,75,175,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [],
        spanGaps: false,
    }
]
};



ajax("/getDatasensorfilter2", "GET",{}, onFetchTempSuccess);

function onFetchTempSuccess(response){
  // hideEle("loader");
  var respData = JSON.parse(response);
  chartConfig.labels = respData.dataPoints.map(dataPoint => moment(dataPoint.created_at).format("HH:mm:ss"));
  chartConfig.datasets[0].data = respData.dataPoints.map(dataPoint => dataPoint.field1);
  renderFirstChart(chartConfig);
}

ajax("/getDatasensor2filter2", "GET",{}, onFetchTempSuccess2);

function onFetchTempSuccess2(response){
  // hideEle("loader");
  var respData = JSON.parse(response);
  chartConfig2.labels = respData.dataPoints.map(dataPoint => moment(dataPoint.created_at).format("HH:mm:ss"));
  chartConfig2.datasets[0].data = respData.dataPoints.map(dataPoint => dataPoint.field2);
  renderSecondChart(chartConfig2);
}

ajax("/getDatasensor3filter2", "GET",{}, onFetchTempSuccess3);

function onFetchTempSuccess3(response){
  // hideEle("loader");
  var respData = JSON.parse(response);
  chartConfig3.labels = respData.dataPoints.map(dataPoint => moment(dataPoint.created_at).format("HH:mm:ss"));
  chartConfig3.datasets[0].data = respData.dataPoints.map(dataPoint => dataPoint.field3);
  renderThirdChart(chartConfig3);
}

ajax("/getDatasensor4filter2", "GET",{}, onFetchTempSuccess4);

function onFetchTempSuccess4(response){
  // hideEle("loader");
  var respData = JSON.parse(response);
  chartConfig4.labels = respData.dataPoints.map(dataPoint => moment(dataPoint.created_at).format("HH:mm:ss"));
  chartConfig4.datasets[0].data = respData.dataPoints.map(dataPoint => dataPoint.field4);
  renderFourthChart(chartConfig4);
}
