//socketio
const socket = io();

//login success
socket.on('login_res',(msg)=>{
    if(msg == 'success'){
        window.location.href='/dashboard';
    }else{
        alert("Login Gagal");
    }
});

//index login submit
$("#submit_login").click(function(){
    var userData = {
        username : $("#login").val(),
        password : $("#password").val()
    };
    socket.emit('login',userData);
    socket.emit('update');
});

//index login enter
var passEnter = document.getElementById("password");
passEnter.addEventListener("keyup",function(event){
    if (event.keyCode === 13) {
        event.preventDefault();
        document.getElementById("submit_login").click(function(){
            var userData = {
                username : $("#login").val(),
                password : $("#password").val()
            };
            socket.emit('login',userData);
            socket.emit('update');
        });
    }
})



