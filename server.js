//import package
var express = require('express');
var http = require('http');
var socketio = require('socket.io');
var path = require('path');
var bodyParser = require('body-parser');
const https = require('https');
var fs = require('fs');

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Datasensor = require('./models/datasensor.js');
const Interval = require('./models/interval.js');
const Login = require('./models/login.js');

//set server
const app = express();
const server = http.createServer(app);
const io = socketio(server);

//setting midleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

//import views
var table_view = fs.readFileSync('views/table_view.html');
var dashboard = fs.readFileSync('views/dasboard.html');
var setting = fs.readFileSync('views/setting.html');
var index = fs.readFileSync('views/index.html');
var temp = fs.readFileSync('views/temp.html');

//variabel data-data sensor
var dashboard_tampil; //default
var data_sensor;
var first_entry_id;
var last_entry_id;
var data_sensor_filter = {
  sensor : "temperature",
  dataPoints: []
}

var data_sensor2_filter = {
  sensor : "pH",
  dataPoints: []
}

var data_sensor3_filter = {
  sensor : "turbidity",
  dataPoints: []
}

var data_sensor4_filter = {
  sensor : "conductivity",
  dataPoints: []
}

//data untuk chart
var data_sensor_filter2 = {
  sensor : "temperature",
  dataPoints: []
}

var data_sensor2_filter2 = {
  sensor : "pH",
  dataPoints: []
}

var data_sensor3_filter2 = {
  sensor : "turbidity",
  dataPoints: []
}

var data_sensor4_filter2 = {
  sensor : "conductivity",
  dataPoints: []
}

var jumlah_data;
var jumlah_data2;
var jumlah_data3;
var jumlah_data4;
var num=0;

//Connecting to database mongoose
//Set up mongo database
// const connectionURL = 'mongodb://127.0.0.1:27017/rivermonitoring';
const connectionURL= 'mongodb+srv://rivermonitoring:pusairdago27%21@cluster0-m196a.mongodb.net/rivermonitoring';
mongoose.connect(connectionURL,{
  useNewUrlParser:true
}).then(()=>{
  console.log("Successfully connected to the database");
  read_datasensor();    
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});


const thingspeakURL='https://api.thingspeak.com/channels/665028/feeds.json?api_key=AFDR8UBHC2POD03N';
var database_last_entry_id;
var jumlah;

//socket io client
io.on('connection',(socket) =>{
  //new client connect
  num++;
  console.log("connected "+ num +" "+ new Date );

  //login
  socket.on('login', (userData)=>{
    console.log('user ',userData);
    Login.findOne({username:userData.username,password:userData.password},(error,result)=>{
      if(error){
        console.log('Unable to find');
        socket.emit('login_res','nofound');
      }
      if(!result){
        socket.emit('login_res','nofound');
      }else{
        socket.emit('login_res','success');
      }
    });
  });


  //client disconnect
  socket.on('disconnect', ()=>{
    console.log('disconnect '+ num +" " + new Date);
  });
});

//router response
app.get('/getDatasensor', function(req,res){
  res.send(data_sensor);
});

app.get('/getDatasensorfilter2', function(req,res){
  res.send(data_sensor_filter2);
});

app.get('/getDatasensor2filter2', function(req,res){
  res.send(data_sensor2_filter2);
});

app.get('/getDatasensor3filter2', function(req,res){
  res.send(data_sensor3_filter2);
});

app.get('/getDatasensor4filter2', function(req,res){
  res.send(data_sensor4_filter2);
});

//router views
app.get('/',function(req,res){
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end(index);
});

app.get('/table', function(req,res){
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end(table_view);
});

app.get('/dashboard', function(req,res){
  read_datasensor();
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end(dashboard);
});

app.get('/setting', function(req,res){
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end(setting);
});

app.get('/temp', function(req,res){
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end(temp);
});

app.get('/getSetting', function(req,res){
  Interval.findById(0,(error,result)=>{
    if(error){
            console.log('Unable to find');
        }
    console.log("setting",result);
    res.send(result);
  });
});

app.get('/getInterval', function(req,res){
  Interval.findById(0,(error,result)=>{
    if(error){
            console.log('Unable to find');
        }
    console.log("interval",result);
    res.send(result);
  });
});

app.get('/api/dbdatasensor',function(req,res){
  Datasensor.find({}).sort({entry_id: 1}).then(eachOne => {
    res.json(eachOne);
  });
});

//router post request
app.post('/input',function(req,res){
  var nilai_sampling = req.body.time;
  var nilai_tampil = req.body.tampil;
  console.log(nilai_sampling, nilai_tampil);
  res.end("SUCCESS");
  Interval.updateOne({_id:0
  },{
    $set:{
      timer: nilai_sampling,
      tampil: nilai_tampil
    }
  }).then((result)=>{
    console.log(result.modifiedCount);
  }).catch((error)=>{
    console.log(error);
  });
});

//server listens to port
server.listen(process.env.PORT || 3000, () =>{
  console.log('Example app listening on port 3000!')
});

//functions
function writedb(){
  Datasensor.insertMany(data_sensor,(error,result)=>{
    if(error){
        return console.log('Unable to insert field');
    }
    //print result that had been sent
    console.log(result.ops);
  });
}

function readdb(){
  Datasensor.findOne().sort({ field: 'asc', _id: -1 }).limit(1).then((result)=>{
    database_last_entry_id=result.entry_id;
    console.log('last entry', result.entry_id);
    if(database_last_entry_id < data_sensor[data_sensor.length -1].entry_id){
      var urutan=data_sensor.length -1 - (data_sensor[data_sensor.length -1].entry_id - database_last_entry_id);
      var data_update=[];
      var num=0;
      for(var k=urutan+1;k<=99;k++){
        console.log('nilai k', k);
        data_update[num]=data_sensor[k];
        num++;
      }
      if(num>0){
        Datasensor.insertMany(data_update,(error,result)=>{
          if(error){
              return console.log('Unable to insert field');
          }
          //print result that had been sent
          console.log(result.ops);
        });
      }
      console.log('Write Data');
    }else{
      console.log('Data match');
    }
  }).catch((err) =>{
    if(err){
      console.log(err);
    }
  });
}

function read_datasensor(){
  https.get(thingspeakURL, (resp) => {
    let data = '';
    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });
      // The whole response has been received. Print out the result.
    resp.on('end', () => {
      data_sensor =JSON.parse(data).feeds;
      first_entry_id=JSON.parse(data).feeds[0].entry_id;
      console.log('first_entry_id '+first_entry_id);
      last_entry_id = JSON.parse(data).channel.last_entry_id;
      console.log('last_entry_id '+last_entry_id+'first_entry_id '+first_entry_id);
      readdb();
      // var i;
      // jumlah_data=0;
      // jumlah_data2=0;
      // jumlah_data3=0;
      // jumlah_data4=0;
      // for (i = 0; i <= (last_entry_id-first_entry_id); i++) {
      //   if (data_sensor[i].field1 != null) {
      //     console.log('Data ke '+data_sensor[i].entry_id+' yaitu '+data_sensor[i].field1);
      //     data_sensor_filter.dataPoints[jumlah_data]=data_sensor[i];
      //     jumlah_data++;
      //   }else if (data_sensor[i].field2!=null) {
      //     data_sensor2_filter.dataPoints[jumlah_data2]=data_sensor[i];
      //     jumlah_data2++;
      //   }else if (data_sensor[i].field3 !=null) {
      //     data_sensor3_filter.dataPoints[jumlah_data3]=data_sensor[i];
      //     jumlah_data3++;
      //   }else if (data_sensor[i].field4 !=null) {
      //     data_sensor4_filter.dataPoints[jumlah_data4]=data_sensor[i];
      //     jumlah_data4++;
      //   }
      // }
      // readdb();
      // //writedb();
      // jumlah=i-1;
      // console.log('last data', data_sensor[jumlah].entry_id);     
  
      // console.log('jumlah data '+jumlah_data);
      // var k=0;
      // for(var i=jumlah_data-dashboard_tampil;i<jumlah_data;i++){
      //   if (i>=0) {
      //     data_sensor_filter2.dataPoints[k]=data_sensor_filter.dataPoints[i];
      //     k++;
      //   } else {
      //     i=0;
      //   }
      // }
      // k=0;
      // for(var i=jumlah_data2-dashboard_tampil;i<jumlah_data2;i++){
      //   if (i>=0) {
      //     data_sensor2_filter2.dataPoints[k]=data_sensor2_filter.dataPoints[i];
      //     k++;
      //   } else {
      //     i=0;
      //   }
      // }
      // k=0;
      // for(var i=jumlah_data3-dashboard_tampil;i<jumlah_data3;i++){
      //   if (i>=0) {
      //     data_sensor3_filter2.dataPoints[k]=data_sensor3_filter.dataPoints[i];
      //     k++;
      //   } else {
      //     i=0;
      //   }
      // }
      // k=0;
      // for(var i=jumlah_data4-dashboard_tampil;i<jumlah_data4;i++){
      //   if (i>=0) {
      //     data_sensor4_filter2.dataPoints[k]=data_sensor4_filter.dataPoints[i];
      //     k++;
      //   } else {
      //     i=0;
      //   }
      // }
      //create database in mongodb
    });
  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });
  data_dashboard();
}

function data_dashboard(){
  Datasensor.find({}).sort({entry_id:1}).then(data_sensor2 => {
    var i;
    jumlah_data=0;
    jumlah_data2=0;
    jumlah_data3=0;
    jumlah_data4=0;
    for (i = 0; i <= (data_sensor2.length - 1); i++) {
      if (data_sensor2[i].field1 != null) {
        console.log('Data ke '+data_sensor2[i].entry_id+' yaitu '+data_sensor2[i].field1);
        data_sensor_filter.dataPoints[jumlah_data]=data_sensor2[i];
        jumlah_data++;
      }else if (data_sensor2[i].field2!=null) {
        data_sensor2_filter.dataPoints[jumlah_data2]=data_sensor2[i];
        jumlah_data2++;
      }else if (data_sensor2[i].field3 !=null) {
        data_sensor3_filter.dataPoints[jumlah_data3]=data_sensor2[i];
        jumlah_data3++;
      }else if (data_sensor2[i].field4 !=null) {
        data_sensor4_filter.dataPoints[jumlah_data4]=data_sensor2[i];
        jumlah_data4++;
      }
    }
    
    //writedb();
    jumlah=i-1;
    console.log('jumlah ',jumlah_data);
    console.log('last data', data_sensor2[jumlah].entry_id);     

    console.log('jumlah data '+jumlah_data);
    Interval.findById(0,(error,result)=>{
      if(error){
              console.log('Unable to find');
          }
      dashboard_tampil = result.tampil;
      data_sensor_filter2.dataPoints = [];
      data_sensor2_filter2.dataPoints = [];
      data_sensor3_filter2.dataPoints = [];
      data_sensor4_filter2.dataPoints = [];
      var k=0;
      for(var i=jumlah_data-dashboard_tampil;i<jumlah_data;i++){
        if (i>=0) {
          data_sensor_filter2.dataPoints[k]=data_sensor_filter.dataPoints[i];
          k++;
        } else {
          i=0;
        }
      }
      k=0;
      for(var i=jumlah_data2-dashboard_tampil;i<jumlah_data2;i++){
        if (i>=0) {
          data_sensor2_filter2.dataPoints[k]=data_sensor2_filter.dataPoints[i];
          k++;
        } else {
          i=0;
        }
      }
      k=0;
      for(var i=jumlah_data3-dashboard_tampil;i<jumlah_data3;i++){
        if (i>=0) {
          data_sensor3_filter2.dataPoints[k]=data_sensor3_filter.dataPoints[i];
          k++;
        } else {
          i=0;
        }
      }
      k=0;
      for(var i=jumlah_data4-dashboard_tampil;i<jumlah_data4;i++){
        if (i>=0) {
          data_sensor4_filter2.dataPoints[k]=data_sensor4_filter.dataPoints[i];
          k++;
        } else {
          i=0;
        }
      }
    });
    
  });
}