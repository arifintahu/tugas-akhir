const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const intervalSchema = new Schema({
    _id: Number,
    timer: String,
    tampil: String
});

const Interval = mongoose.model('Interval', intervalSchema, 'interval');

module.exports = Interval;