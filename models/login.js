const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const loginSchema = new Schema({
    username: String,
    password: String
});

const Login = mongoose.model('Login', loginSchema, 'login');

module.exports = Login;