const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const datasensorSchema = new Schema({
    created_at: String,
    entry_id: Number,
    field1: String,
    field2: String,
    field3: String,
    field4: String
});

const Datasensor = mongoose.model('Datasensor', datasensorSchema,'datasensor');

module.exports = Datasensor;