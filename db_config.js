const mongodb = require('mongodb');

//Set up mongo database
const MongoClient = mongodb.MongoClient;
// const connectionURL= 'mongodb+srv://rivermonitoring:pusairdago27%21@cluster0-m196a.mongodb.net/test?retryWrites=true';
const connectionURL = 'mongodb://127.0.0.1:27017';
const databaseName='rivermonitoring';
var db;

//inisialisasi MongoClient Connection Pooling
MongoClient.connect(connectionURL,{useNewUrlParser:true},{
    poolSize:5
},function(error,client){
    if(error){
      return console.log('Unable to connect');
    }
    console.log('Connect correctly');
    //connect to databaseName
    db = client.db(databaseName);
    module.exports = db;
});



